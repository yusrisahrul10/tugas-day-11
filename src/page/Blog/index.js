import { Box, Container, Grid } from "@mui/material";
import React, { useState, useEffect } from "react";
import { CardView } from "../../assets/components/Card";
import { BoxList } from "../../assets/components/List";
import axios from "axios";
import { useAsync } from "react-use";

export const Blog = () => {
  const api = "http://localhost:3004/postgenerated";
  const fetchData = async () => {
    const response = await axios.get(api);
    return response.data;
  }

  const { value: data, loading, error } = useAsync(fetchData);

  return (
    <Container maxWidth="lg">
      {loading && <p>Loading data...</p>}
      {error && <p>Error fetching data! {error}</p>}
      {data  && (
        <Box sx={{ display: "flex" }} color="primary">
        <Box
          sx={{
            flex: "3 1 0",
            maxWidth: "900px",
            height: 200,
          }}
        >
          <Grid container spacing={2}>
            {(data || []).map((value) => {
              return (
                <Grid item xs={12} md={4} sm={3}>
                  <CardView
                    path={value.img}
                    title={value.title}
                    subheader={value.datePost}
                    description={value.description}
                  />
                </Grid>
              );
            })}
          </Grid>
        </Box>
        <Box
          sx={{
            display: { xs: "none", sm: "none", md: "block" },
            flex: "1 1 0",
            height: 200,
          }}
        >
          <BoxList />
        </Box>
      </Box>
      )
    }
    </Container>
  );
};
