import React from 'react';
import ReactDOM from 'react-dom/client';
import { App } from './App';
import getTheme from "../src/utils/Theme";
import { ThemeProvider } from '@mui/material';

const root = ReactDOM.createRoot(document.getElementById('root'));
const color = process.env.REACT_APP_COLOR_PRIMARY;

root.render(
  <React.StrictMode>
    <ThemeProvider theme={getTheme(color)}>
        <App />
    </ThemeProvider>
  </React.StrictMode>
);
