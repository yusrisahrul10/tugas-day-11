import { createTheme } from '@mui/material/styles'; 

function getTheme(environment) {
  let primaryColor;
  
  if (environment === 'local') {
    primaryColor = '#FF0000'; // merah
  } else if (environment === 'alpha') {
    primaryColor = '#00FF00'; // hijau
  } else if (environment === 'beta') {
    primaryColor = '#0000FF'; // biru
  } else {
    primaryColor = '#000000'; // hitam (default jika environment tidak dikenali)
  }

  const theme = createTheme({
    typography: {
      fontFamily: [
        'Nunito Sans',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif'
      ].join(','),
    },
    palette: {
      primary: {
        main: primaryColor
      }
    }
  });

  return theme;
}

  export default getTheme;